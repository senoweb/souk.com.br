Souk
==========

This is the API repository for the Souk MVC Project, developed with ❤️ and ☕ 

----

Requirements:

- All the [Laravel requirements](https://laravel.com/docs/5.6/installation)
- MySQL Server
- PHP Curl extension
- OpenSSL

----

change to root folder
```
$ cd souk.com.br
```

Install the dependencies
```
$ composer install
```

Copy .env.example to .env
```
$ cp .env.example .env
```

Generate the app key
```
$ php artisan key:generate
```

Grant framework storage permission
```
$ chgrp -R www-data storage bootstrap/cache
$ chmod -R ug+rwx storage bootstrap/cache
```

Create database and run migrations
```
$ create database souk;
$ php artisan migrate
```

Start serve
```
$ php artisan serve --port=8000
```