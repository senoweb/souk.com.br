<?php

namespace App\Entities;

use App\Services\CdnService;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\App;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'document', 'birthday', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $date = [ 'birthday' ];

    protected $appends = [
        'avatar_url'
    ];

    public function getAvatarUrlAttribute() : string
    {
        if($this->avatar) {
            $cdn = new CdnService();

            return $cdn->getResourceUrl($this->avatar);
        }

        return '';
    }
}
