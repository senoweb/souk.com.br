<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CdnService extends Service
{
    protected $cdn;

    public function __construct()
    {
        $this->cdn = new Client([
            'base_uri' => env('CDN_URL'),
            'timeout'  => 10.0,
        ]);
    }

    public function createResource($file) : string
    {
        try {
            $request = $this->cdn->post(
                'image',
                [
                    'multipart' => [
                        [
                            'name' => 'image',
                            'filename' => $file->getClientOriginalName(),
                            'Mime-Type' => $file->getClientMimeType(),
                            'contents' => $file->openFile(),
                        ]
                    ]
                ]
            );

            $resources = json_decode((string)  $request->getBody());

            return $resources->data->id;
        } catch (ClientException $e) {
            return '';
        }
    }

    public function updateResource($resource, $file) : string
    {
        try {
            $request = $this->cdn->post(
                'image/'.$resource,
                [
                    'multipart' => [
                        [
                            'name' => 'image',
                            'filename' => $file->getClientOriginalName(),
                            'Mime-Type' => $file->getClientMimeType(),
                            'contents' => $file->openFile(),
                        ]
                    ]
                ]
            );

            $resources = json_decode((string)  $request->getBody());

            return $resources->data->id;
        } catch (ClientException $e) {
            return '';
        }
    }

    public function getResourceUrl($resource) : string
    {
        try {
            $request = $this->cdn->get(
                'image/'.$resource
            );

            $resources = json_decode((string)  $request->getBody());

            return $resources->data->url;
        } catch (ClientException $e) {
            return '';
        }
    }
}
