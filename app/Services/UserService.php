<?php

namespace App\Services;

use App\Entities\User;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;

class UserService extends Service
{
    protected $page;

    public function __construct(CdnService $cdn)
    {
        $this->cdn = $cdn;
    }

    public function listUsers($search = null, $page = 5)
    {
        $this->page = $page;

        $user = User::orderBy('name');

        if($search)
            $user->orWhere('name', 'like', '%'.$search.'%')->orWhere('email', 'like', '%'.$search.'%')->orWhere('document', 'like', '%'.$search.'%');

        return $user->paginate($this->page)->appends(['search' => $search]);
    }

    public function storeUser($fields)
    {
        $user = User::create([
            'name' => $fields->name,
            'email' => $fields->email,
            'document' => $fields->document,
            'birthday' => $fields->birthday,
            'password' => Hash::make($fields->password),
            'avatar' => '',
        ]);

        if($fields->avatar)
            $user->avatar = $this->cdn->createResource($fields->avatar);

        $user->save();

        return new UserResource($user);
    }

    public function updateUser(User $user, $fields)
    {
        $user->name = $fields->name;
        $user->document = $fields->document;
        $user->email = $fields->email;

        if($fields->password)
            $user->password = Hash::make($fields->password);

        if($fields->avatar)
            $user->avatar = $this->cdn->updateResource($user->avatar, $fields->avatar);

        $user->save();

        return new UserResource($user);
    }
}
