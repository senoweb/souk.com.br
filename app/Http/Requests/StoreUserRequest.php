<?php

namespace App\Http\Requests;

class StoreUserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'document' => 'required|cpf|unique:users,document',
            'email' => 'required|string|email|unique:users,email',
            'birthday' => 'required|date',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
            'avatar' => 'required|image|mimes:jpeg,png,jpg',
        ];
    }
}
