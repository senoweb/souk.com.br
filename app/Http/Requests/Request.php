<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

use App\Exceptions\StandardError;

abstract class Request extends FormRequest
{
    public function authorize()
    {
        return true;
    }
}
