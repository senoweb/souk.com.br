<?php

namespace App\Http\Requests;

class UpdateUserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('user')->id;

        return [
            'name' => 'required|string',
            'document' => 'required|cpf|unique:users,document,'.$id,
            'email' => 'required|string|email|unique:users,email,'.$id,
            'birthday' => 'required|date',
            'password' => 'nullable|confirmed|min:6',
            'password_confirmation' => 'nullable|min:6',
            'avatar' => 'image|mimes:jpeg,png,jpg',
        ];
    }
}

