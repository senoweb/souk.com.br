@extends('template')

@section('main')
    <div class="text-center">
        <h2>Criar usuário</h2>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form no-validate class="form-horizontal" method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                @include('users.form')
            </form>
        </div>
    </div>
@stop