@extends('template')

@section('main')
    <div class="text-center">
        <h2>Lista de usuários</h2>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <form no-validate class="form-horizontal" method="GET" action="{{ route('users.index') }}">
        <div class="form-group">
            <label for="document">Pesquisar</label>
            <input type="text" class="form-control" name="search" id="search" placeholder="Pesquisar" value="{{ ($search) ?: '' }}">
        </div>
    </form>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Cpf</th>
                    <th>Data de nascimento</th>
                    <th>avatar</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->document }}</td>
                        <td>{{ \Carbon\Carbon::parse($user->birthday)->format('d/m/Y') }}</td>
                        <td>
                            @if($user->avatar_url)
                                <button class="avatar btn btn-default" href="{{$user->avatar_url}}">Avatar</button>
                            @endif
                        </td>
                        <td><a class="btn btn-default" role="button" href="{{ route('users.edit', $user->id) }}">Editar</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {{ $users->links() }}
        </div>
    </div>
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" data-dismiss="modal">
            <div class="modal-content"  >
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                     <img src="" class="imagepreview" style="width: 100%;" >
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function() {
            $('.avatar').on('click', function() {
                $('.imagepreview').attr('src', $(this).attr('href'));
                $('#imagemodal').modal('show');
            });
        });
    </script>
@stop