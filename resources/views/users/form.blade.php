{{ csrf_field() }}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name">Nome</label>
    <input type="text" class="form-control" name="name" id="name" placeholder="Nome" value="{{ isset($user) ? $user->name : old('name') }}">
    @if ($errors->has('name'))
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email">E-mail</label>
    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="{{ isset($user) ? $user->email : old('email') }}">
    @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('document') ? ' has-error' : '' }}">
    <label for="document">Cpf</label>
    <input type="text" class="form-control" name="document" id="document" placeholder="Cpf" value="{{ isset($user) ? $user->document : old('document') }}">
    @if ($errors->has('document'))
        <span class="help-block">
            <strong>{{ $errors->first('document') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
    <label for="birthday">Data de nascimento</label>
    <input type="date" class="form-control" name="birthday" id="birthday" placeholder="Data de nascimento" value="{{ isset($user) ? $user->birthday : old('birthday') }}">
    @if ($errors->has('birthday'))
        <span class="help-block">
            <strong>{{ $errors->first('birthday') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label for="password">Senha</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Senha">
    @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
    <label for="password_confirmation">Confirmar senha</label>
    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirmar senha">
    @if ($errors->has('password_confirmation'))
        <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
        </span>
    @endif
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            <label for="avatar">Avatar</label>
            <input type="file" id="avatar" name="avatar" value="{{ isset($user) ? $user->avatar : old('avatar') }}">
            <p class="help-block">Selecione uma imagem para o avatar do usuário.</p>
            @if ($errors->has('avatar'))
                <span class="help-block">
                    <strong>{{ $errors->first('avatar') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-md-6">
            @if(isset($user->avatar_url))
                <img src="{{$user->avatar_url}}" style="width: auto; height: 195px;" class="img-circle" />
            @endif
        </div>
    </div>
</div>
<button type="submit" class="btn btn-default">Salvar</button>